package com.mrfarms.app.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.mrfarms.app.di.viewmodel.ViewModelFactory;
import com.mrfarms.app.di.viewmodel.ViewModelKey;
import com.mrfarms.app.view.onboarding.OnboardingViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


@Module
public abstract class ViewModelModule {

    @Binds
    @SuppressWarnings("unused")
    abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);

    @Binds
    @IntoMap
    @ViewModelKey(OnboardingViewModel.class)
    abstract ViewModel bindsOnboardingViewModel(OnboardingViewModel onboardingViewModel);


}
