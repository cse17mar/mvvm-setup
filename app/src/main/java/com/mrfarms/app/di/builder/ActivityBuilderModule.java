package com.mrfarms.app.di.builder;




import com.mrfarms.app.view.onboarding.OnboardingActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector()
    abstract OnboardingActivity onboardingActivity();

}
