package com.mrfarms.app.di.viewmodel;


import androidx.lifecycle.ViewModel;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import dagger.MapKey;

/**
 * The interface View model key.
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@MapKey
public @interface ViewModelKey {
    /**
     * Value class.
     *
     * @return the class
     */
    @SuppressWarnings("unused")
    Class<? extends ViewModel> value();
}