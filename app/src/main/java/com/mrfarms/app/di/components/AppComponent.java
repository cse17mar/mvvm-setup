package com.mrfarms.app.di.components;

import android.app.Application;

import com.mrfarms.app.MrFarmsApplication;
import com.mrfarms.app.di.builder.ActivityBuilderModule;
import com.mrfarms.app.di.builder.FragmentBuilderModule;
import com.mrfarms.app.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;


@Singleton
@Component(modules = {AppModule.class, AndroidSupportInjectionModule.class, ActivityBuilderModule.class})
public interface AppComponent {

    /**
     * The interface Builder.
     */
    @Component.Builder
    interface Builder {
        /**
         * Application builder.
         *
         * @param application the application
         * @return the builder
         */
        @BindsInstance
        Builder application(Application application);

        /**
         * Build app component.
         *
         * @return the app component
         */
        AppComponent build();
    }

    void inject(MrFarmsApplication tagNearApplication);

}
