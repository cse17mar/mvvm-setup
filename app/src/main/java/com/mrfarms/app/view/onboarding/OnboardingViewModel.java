package com.mrfarms.app.view.onboarding;

import com.mrfarms.app.view.base.BaseRepository;
import com.mrfarms.app.view.base.BaseViewModel;

import javax.inject.Inject;

public class OnboardingViewModel extends BaseViewModel {

    private OnboardingRepository onboardingRepository;

    @Inject
    public OnboardingViewModel(OnboardingRepository repository) {
        this.onboardingRepository = repository;
    }

    @Override
    public BaseRepository getRepository() {
        return onboardingRepository;
    }
}
