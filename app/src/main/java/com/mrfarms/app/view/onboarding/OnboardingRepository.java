package com.mrfarms.app.view.onboarding;

import com.mrfarms.app.retrofit.ApiService;
import com.mrfarms.app.view.base.BaseRepository;

import javax.inject.Inject;

public class OnboardingRepository extends BaseRepository {

    ApiService apiService;

    @Inject
    public OnboardingRepository(ApiService apiService) {
        this.apiService = apiService;
    }
}
