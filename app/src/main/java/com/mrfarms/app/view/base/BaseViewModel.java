package com.mrfarms.app.view.base;

import androidx.lifecycle.ViewModel;

/**
 * Created By mayurlathkar on 29,September,2019
 */
public abstract class BaseViewModel extends ViewModel {

    public abstract BaseRepository getRepository();

    @Override
    protected void onCleared() {
        super.onCleared();
        if (getRepository() != null)
            getRepository().disposeAllDisposables();
    }

}
