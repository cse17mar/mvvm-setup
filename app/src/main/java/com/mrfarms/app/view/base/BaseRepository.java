package com.mrfarms.app.view.base;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BaseRepository {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    public void addDisposable(Disposable disposable) {
        if (this.compositeDisposable != null)
            compositeDisposable.add(disposable);
    }

    public void disposeAllDisposables() {
        if (this.compositeDisposable != null)
            this.compositeDisposable.dispose();
    }

}
