package com.mrfarms.app.view.base;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.mrfarms.app.BuildConfig;
import com.mrfarms.app.R;
import com.mrfarms.app.customView.CustomProgressDialog;
import com.mrfarms.app.models.Error;
import com.mrfarms.app.util.SharedPreference;

import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava2.HttpException;

/**
 * The type Base activity.
 *
 * @param <D> the type parameter
 */
public abstract class BaseActivity<V extends ViewModel, D extends ViewDataBinding> extends AppCompatActivity implements HasSupportFragmentInjector {

    @SuppressWarnings("unused")
    public D dataBinding;
    public V viewModel;
    @Inject
    DispatchingAndroidInjector<Fragment> fragmentAndroidInjector;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private CustomProgressDialog dialogProgress;

    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract Class<V> getViewModel();

    protected abstract void init(Bundle savedInstanceState);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        dataBinding = DataBindingUtil.setContentView(this, getLayoutRes());
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModel());

        SharedPreference.getInstance().initialize(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        init(savedInstanceState);

    }

    public void showToast(String toast) {
        Toast.makeText(this, toast, Toast.LENGTH_SHORT).show();
    }

    public void showSnackBar(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentAndroidInjector;
    }


    public void addReplaceFragment(Fragment fragment, boolean isAdd) {
        String TAG = fragment.getClass().getName();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (!fragmentManager.isStateSaved() && fragmentManager.findFragmentByTag(TAG) == null) {
            if (isAdd) {
                fragmentTransaction.add(R.id.content_frame, fragment, TAG);
                fragmentTransaction.addToBackStack(TAG);
            } else {
                fragmentTransaction.replace(R.id.content_frame, fragment, TAG);
            }
            fragmentTransaction.commitAllowingStateLoss();
            fragmentManager.executePendingTransactions();
        } else {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    /**
     * Replace fragment.
     *
     * @param fragment the fragment
     */
    public void replaceFragment(Fragment fragment) {
        //replacing the fragment
        /*if (fragment != null) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.slide_from_right, R.anim.slide_to_left, R.anim.slide_from_left, R.anim.slide_to_right);
            ft.replace(R.id.content_frame, fragment, fragment.getClass().getName());

            ft.commit();
        }*/
        addReplaceFragment(fragment, false);
    }


    public void handleExceptionThrowables(Throwable throwable) {

        if (throwable != null) {
            showToast(apiExceptionMessage(throwable));
            throwable.printStackTrace();
        } else
            showToast(getString(R.string.something_went_wrong));

    }

    public String apiExceptionMessage(Throwable throwable) {

        if (throwable == null)
            return getString(R.string.something_went_wrong);
        else if (throwable instanceof HttpException) {
            ResponseBody responseBody = ((HttpException) throwable).response().errorBody();
            return (getErrorMessage(responseBody));
        } else if (throwable instanceof SocketTimeoutException) {
            return "Sorry! timeout, please check your internet connection";
        } else if (throwable instanceof IOException) {
            return getString(R.string.something_went_wrong);
        } else {
            return (throwable.getMessage());
        }
    }

    private String getErrorMessage(ResponseBody responseBody) {
        try {

            JSONObject jsonObject = new JSONObject(responseBody.string());

            if (jsonObject.get("error") != null && jsonObject.get("error") instanceof JSONObject) {
                Error error = new Gson().fromJson(jsonObject.getJSONObject("error").toString(), Error.class);
                log("error response", error.getDescription());
                return error.getDescription();
            } else if (jsonObject.get("error") != null && jsonObject.get("error") instanceof String && jsonObject.has("message")) {
                return jsonObject.getString("message");

            } else {
                return getString(R.string.something_went_wrong);
            }


        } catch (Exception e) {
            e.printStackTrace();
            return getString(R.string.something_went_wrong);
        }
    }

    public final void log(String key, String message) {
        if (BuildConfig.DEBUG)
            Log.e(key, message);
    }


    /**
     * Show progress.
     */
    public void showProgress() {
        if (dialogProgress == null) {
            dialogProgress = new CustomProgressDialog(this);
        }
        if (!dialogProgress.isShowing())
            dialogProgress.show();
    }

    /**
     * Hide progress.
     */
    public final void hideProgress() {
        if (dialogProgress != null)
            dialogProgress.cancel();
    }

}
