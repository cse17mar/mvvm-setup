package com.mrfarms.app.view.base;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import okhttp3.ResponseBody;

/**
 * The type Base fragment.
 *
 * @param <V> the type parameter
 * @param <D> the type parameter
 */
public abstract class BaseFragment<V extends ViewModel, D extends ViewDataBinding> extends Fragment {

    /**
     * The View model.
     */
    protected V viewModel;
    /**
     * The Data binding.
     */
    protected D dataBinding;
    /**
     * The View model factory.
     */
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private BaseActivity activity;

    /**
     * Gets view model.
     *
     * @return the view model
     */
    protected abstract Class<V> getViewModel();

    /**
     * Gets layout res.
     *
     * @return the layout res
     */
    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract void init(Bundle savedInstanceState);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModel());
        if (getActivity() instanceof BaseActivity)
            this.activity = ((BaseActivity) getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);

        dataBinding.setLifecycleOwner(this);

        init(savedInstanceState);

        return dataBinding.getRoot();
    }

    public void showToast(String toast) {
        Toast.makeText(getContext(), toast, Toast.LENGTH_SHORT).show();
    }

    public void showProgress() {
        if (activity != null)
            activity.showProgress();
    }

    /**
     * Hide progress.
     */
    public void hideProgress() {
        if (activity != null)
            activity.hideProgress();
    }

    public void handleExceptionThrowables(Throwable throwable) {
        if (activity != null)
            activity.handleExceptionThrowables(throwable);
    }

    public void handleErrorMessage(ResponseBody responseBody) {
    }


}
