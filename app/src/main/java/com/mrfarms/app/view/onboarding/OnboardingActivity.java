package com.mrfarms.app.view.onboarding;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.mrfarms.app.R;
import com.mrfarms.app.databinding.ActivityOnboardingBinding;
import com.mrfarms.app.view.base.BaseActivity;

public class OnboardingActivity extends BaseActivity<OnboardingViewModel, ActivityOnboardingBinding> {


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_onboarding;
    }

    @Override
    protected Class<OnboardingViewModel> getViewModel() {
        return OnboardingViewModel.class;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }
}
