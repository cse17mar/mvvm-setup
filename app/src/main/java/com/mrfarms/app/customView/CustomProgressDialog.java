package com.mrfarms.app.customView;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.mrfarms.app.R;


/**
 * The type Custom progress dialog.
 */
public class CustomProgressDialog extends Dialog {

    /**
     * Instantiates a new Custom progress dialog.
     *
     * @param context the context
     */
    public CustomProgressDialog(Context context) {
        super(context);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = (View) LayoutInflater.from(getContext()).inflate(R.layout.item_custom_progressbar, null);
        setContentView(view);
        this.setCanceledOnTouchOutside(false);

    }

    /**
     * Show progress.
     */
    public void showProgress() {
        this.show();
    }

    /**
     * Hide progress.
     */
    public void hideProgress() {
        this.cancel();
    }

}
