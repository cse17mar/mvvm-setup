package com.mrfarms.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

public class SharedPreference {
    private static SharedPreference mInstance;
    private SharedPreferences sharedPreferences;

    private SharedPreference() {

    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static SharedPreference getInstance() {
        if (mInstance == null) {
            mInstance = new SharedPreference();

        }
        return mInstance;
    }

    /**
     * Gets shared preferences.
     *
     * @return the shared preferences
     */
    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    /**
     * Initialize.
     *
     * @param mContext the m context
     */
    public void initialize(Context mContext) {

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    /**
     * Put string.
     *
     * @param key   the key
     * @param value the value
     */
    public void putString(String key, String value) {
        SharedPreferences.Editor e = sharedPreferences.edit();
        e.putString(key, value);
        e.apply();
    }

    /**
     * Put object.
     *
     * @param key   the key
     * @param value the value
     */
    public void putObject(String key, Object value) {
        SharedPreferences.Editor e = sharedPreferences.edit();
        e.putString(key, new Gson().toJson(value));
        e.apply();
    }

    /**
     * Gets string.
     *
     * @param key the key
     * @return the string
     */
    public String getString(String key) {

        return sharedPreferences.getString(key, "");
    }

    /**
     * Gets auth token.
     *
     * @return the auth token
     */

}
