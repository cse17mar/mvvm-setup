package com.mrfarms.app.util;

import com.google.gson.Gson;
import com.mrfarms.app.retrofit.Constants;


public class GsonBuilder {
    private static GsonBuilder ourInstance;
    private com.google.gson.GsonBuilder gsonBuilder;
    private Gson gson;

    private GsonBuilder() {
    }

    public static GsonBuilder getInstance() {

        if (ourInstance == null)
            ourInstance = new GsonBuilder();
        return ourInstance;
    }

    public com.google.gson.GsonBuilder getGsonBuilder() {
        if (gsonBuilder == null) {
            gsonBuilder = new com.google.gson.GsonBuilder()
                    .setDateFormat(Constants.DEFAULT_TIME_FORMAT);
        }
        return gsonBuilder;
    }

    public Gson getGson() {
        if (gson == null)
            gson = getGsonBuilder().create();
        return gson;
    }


}
