package com.mrfarms.app.models;

public class ObjectError {

    private String defaultMessage;

    /**
     * Gets default message.
     *
     * @return the default message
     */
    public String getDefaultMessage() {
        return defaultMessage;
    }

    /**
     * Sets default message.
     *
     * @param defaultMessage the default message
     */
    public void setDefaultMessage(String defaultMessage) {
        this.defaultMessage = defaultMessage;
    }

}
