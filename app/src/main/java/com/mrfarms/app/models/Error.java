package com.mrfarms.app.models;

import java.util.Date;
import java.util.List;


public class Error {

    private String message = "No errors";
    private String description = "No errors found";
    private Date date = new Date();
    private List<ObjectError> allErrors = null;

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     *
     * @param message the message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Instantiates a new Error.
     *
     * @param message     the message
     * @param description the description
     * @param date        the date
     * @param allErrors   the all errors
     */
    public Error(String message, String description, Date date, List<ObjectError> allErrors) {
        this.message = message;
        this.description = description;
        this.date = date;
        this.allErrors = allErrors;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Instantiates a new Error.
     */
    public Error() {
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(Date date) {
        this.date = date;

    }

    /**
     * Gets all errors.
     *
     * @return the all errors
     */
    public List<ObjectError> getAllErrors() {
        return allErrors;
    }

    /**
     * Sets all errors.
     *
     * @param allErrors the all errors
     */
    public void setAllErrors(List<ObjectError> allErrors) {
        this.allErrors = allErrors;
    }

}
