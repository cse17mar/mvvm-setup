package com.mrfarms.app;

import android.app.Activity;
import android.app.Service;

import androidx.multidex.MultiDexApplication;


import com.mrfarms.app.di.components.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;

public class MrFarmsApplication extends MultiDexApplication implements HasActivityInjector, HasServiceInjector {
    private static MrFarmsApplication mInstance;

    /**
     * The Activity dispatching injector.
     */
    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingInjector;
    /**
     * The Dispatching service injector.
     */
    @Inject
    DispatchingAndroidInjector<Service> dispatchingServiceInjector;

    public static MrFarmsApplication getmInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        initApplicationGraph();
    }

    private void initApplicationGraph() {
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return dispatchingServiceInjector;
    }

}
