package com.mrfarms.app.retrofit;

public class Constants {

    /**
     * The constant CALENDAR_PICKER_RESULT.
     */
    public static final int CALENDAR_PICKER_RESULT = 2018;
    /**
     * The constant DEFAULT_TIME_FORMAT.
     */
    public static final String DEFAULT_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    /**
     * The constant AuthToken.
     */
    public static final String AuthToken = "Authorization";
    /**
     * The constant USER_ID.
     */
    public static final String USER_ID = "user_id";
    /**
     * The constant FCM_TOKEN.
     */
    public static final String FCM_TOKEN = "fcm_token";
    /**
     * The constant GET_USER_DETAILS.
     */
    public static final String GET_USER_DETAILS = "api/user";
    /**
     * The constant USER_LOCATION.
     */
    public static final String USER_LOCATION = "USER_LOCATION";

    /**
     * The constant USER.
     */
    public static final String USER = "USER_OBJECT";
    /**
     * The constant USER_TOKEN.
     */
    public static final String USER_TOKEN = "USER_TOKEN";

    /**
     * The constant SHOW_PROGRESS.
     */
    public static final String SHOW_PROGRESS = "show_progress";

    /**
     * The constant HIDE_PROGRESS.
     */
    public static final String HIDE_PROGRESS = "hide_progress";


    /**
     * The constant SUCCESS_RESPONSE.
     */
    public static final int SUCCESS_RESPONSE = 200;


    public static final String CANCEL_DAILOG = "CANCEL_DAILOG";
    public static final String APPLY_FILTER= "APPLY_FILTER";

    public static final String BACK_PRESS= "BACK_PRESS";
    public static final String FILTER_CLICKED= "FILTER_CLICKED";
    public static final String VIEW_CART_CLICKED= "VIEW_CART_CLICKED";

}
