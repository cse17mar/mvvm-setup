package com.mrfarms.app.retrofit;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

public class FileUploadRequestBody extends RequestBody {
    private File mFile;
    private String mPath;
    private UploadCallbacks mListener;
    private Context context;
    private static final int DEFAULT_BUFFER_SIZE = 2048;

    /**
     * The interface Upload callbacks.
     */
    public interface UploadCallbacks {
        /**
         * On progress update.
         *
         * @param percentage the percentage
         */
        void onProgressUpdate(int percentage);

        /**
         * On error.
         */
        void onError();

        /**
         * On finish.
         */
        void onFinish();
    }

    /**
     * Instantiates a new File upload request body.
     *
     * @param file     the file
     * @param listener the listener
     */
    public FileUploadRequestBody(final File file, final UploadCallbacks listener) {
        mFile = file;
        mListener = listener;
    }

    @Override
    public MediaType contentType() {
        // i want to upload only images
//        return MediaType.parse("image/*");
        String mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(mFile).toString()).toLowerCase());
        if (mimetype == null) {
            mimetype = "*/*";
        }

        return MediaType.parse(mimetype);
    }

    @Override
    public long contentLength() throws IOException {
        return mFile.length();
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        long fileLength = mFile.length();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        FileInputStream in = new FileInputStream(mFile);
        long uploaded = 0;

        try {
            int read;
            Handler handler = new Handler(Looper.getMainLooper());
            while ((read = in.read(buffer)) != -1) {

                // update progress on UI thread
                handler.post(new ProgressUpdater(uploaded, fileLength));

                uploaded += read;
                sink.write(buffer, 0, read);
            }
        } finally {
            in.close();
        }
    }

    private class ProgressUpdater implements Runnable {
        private long mUploaded;
        private long mTotal;

        /**
         * Instantiates a new Progress updater.
         *
         * @param uploaded the uploaded
         * @param total    the total
         */
        public ProgressUpdater(long uploaded, long total) {
            mUploaded = uploaded;
            mTotal = total;
        }

        @Override
        public void run() {

            mListener.onProgressUpdate((int) (100 * mUploaded / mTotal));
        }
    }
}