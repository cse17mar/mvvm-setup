package com.mrfarms.app.retrofit;

/**
 * The type Api constants.
 */
public class ApiConstants {

    /**
     * The constant CONNECT_TIMEOUT.
     */
    public static final long CONNECT_TIMEOUT = 30000;
    /**
     * The constant READ_TIMEOUT.
     */
    public static final long READ_TIMEOUT = 30000;
    /**
     * The constant WRITE_TIMEOUT.
     */
    public static final long WRITE_TIMEOUT = 30000;

    public static final String BASE_URL ="http://cwjkbcweb";


}
