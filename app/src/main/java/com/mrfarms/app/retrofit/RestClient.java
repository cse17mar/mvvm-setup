package com.mrfarms.app.retrofit;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.mrfarms.app.util.LocalDateDeserializer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mrfarms.app.BuildConfig.DEBUG;


/**
 * The type Rest client.
 */
public class RestClient {

    private static ApiService service;


    /**
     * Instantiates a new Rest client.
     *
     * @param url                the url
     * @param timeZoneConversion
     */
    public RestClient(String url, boolean timeZoneConversion) {

        try {

            //LOGGING INTERCEPTOR
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            //REQUEST INTERCEPTOR FOR USER LOCATION
            Interceptor interceptor = chain -> {


                Request request = chain.request().newBuilder().addHeader("location_lat", "").addHeader("location_lng", "").build();
                okhttp3.Response response = chain.proceed(request);
                // todo deal with the issues the way you need to
                if (response.code() == 500) {
                    return response;
                }
                return response;
            };


            //OK HTTP CLIENT
            OkHttpClient okHttpClient;

            if (DEBUG) {
                okHttpClient = new OkHttpClient().newBuilder()
                        .connectTimeout(300, TimeUnit.SECONDS)
                        .readTimeout(300, TimeUnit.SECONDS)
                        .writeTimeout(300, TimeUnit.SECONDS)
                        .addInterceptor(interceptor)
                        .addInterceptor(httpLoggingInterceptor)
                        .build();
            } else {
                okHttpClient = new OkHttpClient().newBuilder()
                        .connectTimeout(300, TimeUnit.SECONDS)
                        .readTimeout(300, TimeUnit.SECONDS)
                        .writeTimeout(300, TimeUnit.SECONDS)
                        .addInterceptor(interceptor)
                        .build();
            }


            //GSON BUILDER
            GsonBuilder gsonBuilder = com.mrfarms.app.util.GsonBuilder.getInstance().getGsonBuilder();


            if (timeZoneConversion)//time conversion
                gsonBuilder.registerTypeAdapter(Date.class, new LocalDateDeserializer());
            else {
                gsonBuilder.registerTypeAdapter(Date.class, (JsonDeserializer<Date>) (json, typeOfT, context) -> {
                    String date = json.getAsString();
                    @SuppressLint("SimpleDateFormat")
                    SimpleDateFormat formatter = new SimpleDateFormat(Constants.DEFAULT_TIME_FORMAT);
                    try {
                        return formatter.parse(date);
                    } catch (ParseException e) {
                        Log.d("date conversion error", e.getMessage());
                        return null;
                    }
                });
            }
            Gson gson = gsonBuilder.create();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();


            service = retrofit.create(ApiService.class);
        } catch (Exception e) {
            Log.e("Exception: ", "Error " + e.getMessage());
        }
    }

    /**
     * Get api.
     *
     * @return the api
     */
    public ApiService get() {

        return service;
    }


}
